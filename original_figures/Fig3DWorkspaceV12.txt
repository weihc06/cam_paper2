C:\Users\Hongchuan\Documents\MyResearch\Project_MuriCamera\Code_Figure\LISC_DDPGP_API_Cam_Git\result_moo_cam\12
created by main.m
with rng(23)
at sim_env.t == 7
----
settings:
test_case = 12
flag_floor_plan = 1
flag_plot_3d_model = 0
flag_plot_display_db_cam = 0
flag_plot_display_db_cam_arrow = 1
flag_plot_ticks = 0
flag_sketchup = 1
h_lt3 = light('Position',[17 0 3],'Style','infinite');
view(82,21);
COLOR_SEQ2 = [
    rgb('Gold');
    rgb('Gold');
    rgb('Gold');
    0.9290    0.6940    0.1250
    0.9290    0.6940    0.1250
    0.9290    0.6940    0.1250
    0.8500    0.3250    0.0980
    0.8500    0.3250    0.0980
    0.8500    0.3250    0.0980
    0.8500    0.3250    0.0980
    0.6350    0.0780    0.1840
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0    0.4470    0.7410
    0.4660    0.6740    0.1880
    0.4940    0.1840    0.5560
    0.3010    0.7450    0.9330];